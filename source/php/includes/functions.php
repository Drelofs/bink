<?php

//Convert date  to Dutch format
function format_date($date){
    setlocale(LC_TIME, 'NL_nl');    
    setlocale(LC_ALL, 'nl_NL');
    $newDate = strftime('%e %B %Y %k:%M', strtotime($date));
    return $newDate;
}