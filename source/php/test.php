<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

div:hover 
{
    left: -10px;
    position: relative;
}

a{
    position: relative;
    padding-left: 20px;
}

a:after {
    content: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAPFBMVEX///8AAABxcXEGBgaamprS0tLPz89aWlpWVlbT09NRUVFVVVVZWVldXV1OTk7W1ta2traWlpZGRkaCgoJyyOvBAAACf0lEQVR4nO3dC3KCMBhFYf8+1L5sbfe/14ZxmIoIJLE2vdfzbaCcCY8gkK5WAAAAAAAAAAAAAAAAAAAAAAAAaOpj3XoLrm0fm8fW23BdDxHx/N56K66pKwzrcTwURrzZHo99oW/jT2HE1nJfPS5M55yn1tvz+4aFjvvqaWFqNNtXx4Vu145zhV5zgPOFEa824zhV6HPOmS50aZwr9JgDzBc6zAGWCvX31eVC9TlATqH2HCCvMGInOwfILdSdA+QXqp5zSgo1G8sKI17kro+lhXpzgPLCdM6R2ldrCi+bA9z9sc+qwkvGsfIPNrCrPB7vW294gbp7K6XCNF+t2Fe1CmvmAGqF6fpYuK/qFaZzTlGjYmHZtUOzsOSco1qYP466hbnHo3JhurfKGEftwpzjUb1wuVG/MM0BZhsdCufvOzwK033H5Di6FE4fjz6FU+PoVHj+ePQqPDcHcCscH49+haeNjoXDOYBn4fF9h2vhz7XDt7C/R3YuPFw7vAsjvswLt+Zj6H4cup9Ld2vv6+Hxsw3HQvd56elv4W6F4+fEXoXu9/juv9O4/9Y2/azNo3DuOZtD4fwzNv1C92dP7s8Pp5/HeBTmvcunW+j+Lob7+zTu70S5v9f2Yv5uovv7pe7vCOdc38dab3W+2u/1/L+3+Gt138zUHH+t1BQq9dUUll3f2+P7wyH3b0i1jr8e33L3dNfkyStUHb8O62Jor4nRWVyfRrxvqbD0/v0/uuV1ojz6bne9Nt3r+9gtrpvocvz1Tgu9xq8zLNS7v102WEfYsG+wFrTd/nlwK+t5byz3zwP/dfX31uPX8f//FgAAAAAAAAAAAAAAAAAAAAAAAP/cN+LxOzmMTdyOAAAAAElFTkSuQmCC);
    display: block;
    position: absolute;
    left: 234px;     
    top: -30px; 
    opacity: 0;
    transition: opacity 0.1s ease-out 0s;    
}

a:hover:after {
    transition: opacity 1.5s ease-out 0s;
    opacity: 1;
}

</style>
</head>
<body>

<div class="link-container">
    <a class="link" href="workshops.php">More workshops</a>
</div>  

</body>
</html>