<?php 
$page = 'workshops.php';
include_once 'includes/dbh.inc.php';
include_once 'includes/connect.php';
include_once 'includes/functions.php';
session_start();
$_SESSION['registered'] = false;

$id = $_GET['id'];
try {
    $query = $db->prepare("
                SELECT * FROM `workshops` WHERE `id` = '$id'");

    $query->execute();
} catch (PDOException $e) {
    $sMsg = "<p>
            Line: " . $e->getLine() . "<br />
            File: " . $e->getFile() . "<br />
            Message: " . $e->getMessage() . "
            </p>";

    trigger_error($sMsg);
}

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $data = [
        'id' => $row['id'],
        'title' => $row['title'],
        'date' => format_date($row['date']),
        'address' => $row['address'],
        'location' => $row['location'],
        'content' => $row['content'],
        'filepath' => str_replace("../", "", $row['filepath'])
    ];
}
$address = $data['address'];
$prepAddr = str_replace(' ','+',$address);
$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ');
$output= json_decode($geocode);
$latitude = str_replace(',', '.', floatval($output->results[0]->geometry->location->lat));
$longitude = str_replace(',', '.', floatval($output->results[0]->geometry->location->lng));
$page_title = $data['title'];

$header_image = $data['filepath'];

include_once 'header.php';


?>
<section class="date-location">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <div class="grid-x date-location-block">
                <div class="cell small-12 medium-6 date-left"><b>Datum en tijd: </b><?= $data['date'] ?></div>
                <!-- <div class="cell auto"></div> -->
                <div class="cell small-12 medium-6 location-right"><b>Locatie: </b><?= $data['location'] ?></div>
            </div>
        </div>
    </div>
</section>
<section class="text-block">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <div class="text-block-container">
            <?= $data['content'] ?>
            </div>
        </div>
    </div>
</section>
<section class="google-maps-block" id="map"></section>
<section class="form-block">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <h2>Aanmelden</h2>
            <?php if($_SESSION['registered'] == false) { ?>
            <form method="post" action="register_workshop.php">
                <div class="grid-x">
                    <div class="small-12 medium-6 large-6">
                        <label for="titel">Naam</label>
                        <input type="text" name="name"placeholder="Naam"><br>
                    </div>
                    <div class="small-12 medium-6 large-6">
                        <label for="datum">E-mail</label>
                        <input type="text" name="email"><br>
                    </div>
                </div>
                <div class="grid-x">
                    <div class="small-12 medium-6 large-6">
                        <label for="file">Linkedin ervaring</label>
                        <input type="text" name="level">
                    </div>
                    <div class="small-12 medium-6 large-6">
                        <label for="tel">Telefoonnummer</label>
                        <input type="tel" name="phone" minlength="10" maxlength="10">
                        <input type="hidden" name="workshop_id" value="<?= $data['id'] ?>">
                    </div>
                </div>
                <div class="grid-x">
                    <div class="small-12 medium-4 medium-offset-8 large-4 large-offset-8">
                        <input type="submit" name="submit" value="Aanmelden">
                    </div>
                </div>
            </form>
            <?php }
            else{
                echo "Registratie voltooid";
            } ?>
        </div>
    </div>
</section>
<script>
    var longitude = parseFloat("<?= $longitude ?>");
    var latitude = parseFloat("<?= $latitude ?>");

</script>

<?php 
$_SESSION['registered'] = false;
include_once 'footer.php';