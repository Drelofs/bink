<?php 
$page = 'workshops.php';
$page_title = 'Opkomende workshops';
include_once 'header.php';
?>
<section class="workshops">
    <div class="grid-x">
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/rotterdam.jpg')">
                        <span class="workshop-item-location">Rotterdam</span>
                        <span class="workshop-item-date">28 december 2018</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/amsterdam.jpg')">
                        <span class="workshop-item-location">Amsterdam</span>
                        <span class="workshop-item-date">5 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Advanced</h3>
                            <p>Een workshop over het geadvanceerd gebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/denhaag.jpg')">
                        <span class="workshop-item-location">Den Haag</span>
                        <span class="workshop-item-date">17 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/rotterdam.jpg')">
                        <span class="workshop-item-location">Rotterdam</span>
                        <span class="workshop-item-date">28 december 2018</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/amsterdam.jpg')">
                        <span class="workshop-item-location">Amsterdam</span>
                        <span class="workshop-item-date">5 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Advanced</h3>
                            <p>Een workshop over het geadvanceerd gebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/denhaag.jpg')">
                        <span class="workshop-item-location">Den Haag</span>
                        <span class="workshop-item-date">17 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/rotterdam.jpg')">
                        <span class="workshop-item-location">Rotterdam</span>
                        <span class="workshop-item-date">28 december 2018</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/amsterdam.jpg')">
                        <span class="workshop-item-location">Amsterdam</span>
                        <span class="workshop-item-date">5 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Advanced</h3>
                            <p>Een workshop over het geadvanceerd gebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/denhaag.jpg')">
                        <span class="workshop-item-location">Den Haag</span>
                        <span class="workshop-item-date">17 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/rotterdam.jpg')">
                        <span class="workshop-item-location">Rotterdam</span>
                        <span class="workshop-item-date">28 december 2018</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/amsterdam.jpg')">
                        <span class="workshop-item-location">Amsterdam</span>
                        <span class="workshop-item-date">5 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Advanced</h3>
                            <p>Een workshop over het geadvanceerd gebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/denhaag.jpg')">
                        <span class="workshop-item-location">Den Haag</span>
                        <span class="workshop-item-date">17 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

</section>
<?php include_once 'footer.php';