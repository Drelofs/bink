<?php 
$login_page = true;
$add_edit = false;
include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once 'admin_header.php';
session_start();

if(isset($_POST['submit'])){
    $givenUsername = htmlspecialchars($_POST['username']);
    $givenPassword = htmlspecialchars($_POST['password']);

    $fetch = $db->prepare("SELECT * FROM `accounts` WHERE  `username` = :username");
    $fetch->bindParam(':username', $givenUsername);
    $fetch->execute();
    if (!$fetch->rowCount()) {
        $_SESSION['error'] = 'Gebruikersnaam en wachtwoord komen niet overeen';
        header('location: index.php');
    }
    foreach ($fetch->fetchAll() as $row) {
        $username = $row['username'];
        $password = $row['password'];

        //Check if password is same as in database
        if (password_verify($givenPassword, $password)) {
            unset($givenPassword);

            //Add session variables
            $_SESSION['id'] = $row['id'];
            $_SESSION['username'] = $row['username'];
            $_SESSION['loggedin'] = TRUE;
            $_SESSION['name'] = $row['name'];
            
            header('Location:admin.php');

        }
        else{
            header('Location: login.php');
        }
    }
}
else{
?>
<section class="login-page">
    <div class="login-page-panel">
        <h2>Inloggen</h2>
        <form method="post">
        <div class="login-page-panel-fields">
            <input type="text" name="username" placeholder="Username">
            <input type="password" name="password" placeholder="Password">
            <input type="submit" name="submit" class="login-button" value="Inloggen">
        </div>
        </form>
    </div>
</section>
<?php } 
 include_once 'admin_footer.php'; ?>
