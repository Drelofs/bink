<?php
include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';


if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $username = strtolower($_POST['username']);
    $password = $_POST['password'];

    if($password) {
        $password_hash = password_hash($password, PASSWORD_DEFAULT);
        // print_r("gaat goed");
        try {
            $sql = "INSERT INTO `accounts` (`name`, `username`, `password`) VALUES (?,?,?)";
            $query = $db->prepare($sql);
            $query->execute([$name, $username, $password_hash]);
            echo "<script type='text/javascript'> location.replace('index.php'); </script>";

        } catch (PDOException $e) {
            $sMsg = '<p>
                Line: ' . $e->getLine() . '<br/>
                File: ' . $e->getFile() . '<br/>
                Message: ' . $e->getMessage() . '
                </p>';

            trigger_error($sMsg);
        }
    }
    else{
        $message = "Uw gegevens zijn niet correct.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        echo "<script type=\"text/javascript\"> location.replace(\"register.php\"); </script>";


    }
}

else { ?>
    <div class='loginform'>
<form class='form-horizontal' name='register' method='post'>
    <fieldset>
        <legend>Registreren</legend>
        <div class='form-group'>
            <label for='name' class='col-lg-2 control-label'>Naam</label>
            <div class='col-lg-10'>
                <input type='text' class='form-control' name='name' placeholder='Name'>
            </div>
        </div>
        <div class='form-group'>
            <label for='username' class='col-lg-2 control-label'>Username</label>
            <div class='col-lg-10'>
                <input type='text' class='form-control' name='username' placeholder='Name'>
            </div>
        </div>
        <div class='form-group'>
            <label for='inputPassword' class='col-lg-2 control-label'>Password</label>
            <div class='col-lg-10'>
                <input type='password' class='form-control' id='inputPassword' placeholder='Password' name='password'>
            </div>
        </div>
        <div class='form-group'>
            <div class='col-lg-10 col-lg-offset-2'>
                <input type='submit' name="submit" class='btn btn-primary' value="Submit">
            </div>
        </div>
    </fieldset>
</form>
</div>
<?php } ?>