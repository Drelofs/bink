<?php
include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once '../includes/functions.php';

$id = $_GET["id"];

try {
    $query = $db->prepare("
            DELETE FROM `workshops` WHERE `id` = '$id'");
    $query->execute();

    echo "<script type='text/javascript'> location.replace('admin.php'); </script>";
} catch (PDOException $e) {
    $sMsg = '<p>
        Line: ' . $e->getLine() . '<br/>
        File: ' . $e->getFile() . '<br/>
        Message: ' . $e->getMessage() . '
        </p>';

    trigger_error($sMsg);
}