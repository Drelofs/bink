<script src="../js/vendor.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pwrr3nti5avcjbprgp1t15o9fza69ze0bt4i4cse2bjeevc4"></script>
<?php if($add_edit){ ?>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ&callback=initEditMap&language=nl&region=NL">
</script>
<?php } else { ?>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ&callback=initMap">
</script>
<?php } ?>
<script src="../js/scripts.min.js"></script>
</body>
</html>