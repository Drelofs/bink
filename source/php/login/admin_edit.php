<?php 
$add_edit = true;
$login_page = false;

include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once '../includes/functions.php';
include_once 'admin_header.php';

$uploadDirectory = "../uploads/";

if(isset($_POST['submit'])){
    $id = $_POST['id'];
    $title = $_POST['title'];
    $date = $_POST['date'];
    $content = $_POST['content'];
    $location = $_POST['location'];
    $address = $_POST['address'];

    //Check if a new image is uploaded
    if($_FILES['myfile']['name']){
        $errors = []; // Store all foreseen and unforseen errors here

        $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions

        $fileName = $_FILES['myfile']['name'];
        $fileSize = $_FILES['myfile']['size'];
        $fileTmpName  = $_FILES['myfile']['tmp_name'];
        $fileType = $_FILES['myfile']['type'];
        $fileExtension = strtolower(end(explode('.',$fileName)));

        $imagePath = $uploadDirectory . basename($fileName);
        $uploadPath = $currentDir . $uploadDirectory; 

        // print_r($imagePath);
        // exit();


        if (! in_array($fileExtension,$fileExtensions)) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
        }

        if ($fileSize > 20000000) {
            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }
        // print_r($errors);
        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadDirectory.$fileName);
            // print_r("Did it upload?");

        } else {
            foreach ($errors as $error) {
                // echo $error . "These are the errors" . "\n";
            }
        }
        //Update workshop with new image
        try {
            $query = $db->prepare("
                    UPDATE `workshops` SET `title` = :title, `date` = :date, `address` = :address, `content` = :content, `location` = :location, `filepath` = :filepath WHERE `id` =$id");
            $query->bindParam(':title', $title);
            $query->bindParam(':date', $date);
            $query->bindParam(':address', $address);
            $query->bindParam(':content', $content);
            $query->bindParam(':location', $location);
            $query->bindParam(':filepath', $imagePath);
            $query->execute();
            echo "<script type=\"text/javascript\"> location.replace('admin.php'); </script>";
        } catch (PDOException $e) {
            $sMsg = '<p>
                Line: ' . $e->getLine() . '<br/>
                File: ' . $e->getFile() . '<br/>
                Message: ' . $e->getMessage() . '
                </p>';

            trigger_error($sMsg);
        }
    }
    else{
        //Update workshop with no new image
        try {
            $query = $db->prepare("
                    UPDATE `workshops` SET `title` = :title, `date` = :date, `address` = :address, `content` = :content, `location` = :location WHERE `id` =$id");
            $query->bindParam(':title', $title);
            $query->bindParam(':date', $date);
            $query->bindParam(':address', $address);
            $query->bindParam(':content', $content);
            $query->bindParam(':location', $location);
            $query->execute();
            echo "<script type=\"text/javascript\"> location.replace('admin.php'); </script>";
        } catch (PDOException $e) {
            $sMsg = '<p>
                Line: ' . $e->getLine() . '<br/>
                File: ' . $e->getFile() . '<br/>
                Message: ' . $e->getMessage() . '
                </p>';

            trigger_error($sMsg);
        }
    }
}
else { 
    //Retrieve all data of said workshop
    $id = $_GET['id'];
    try {
        $query = $db->prepare("
                    SELECT * FROM `workshops` WHERE `id` = '$id'");
    
        $query->execute();
    } catch (PDOException $e) {
        $sMsg = "<p>
                Line: " . $e->getLine() . "<br />
                File: " . $e->getFile() . "<br />
                Message: " . $e->getMessage() . "
                </p>";
    
        trigger_error($sMsg);
    }
    
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $data = [
            'id' => $row['id'],
            'title' => $row['title'],
            'date' => $row['date'],
            'content' => $row['content'],
            'address' => $row['address'],
            'location' => $row['location']
        ];

        //Get Google Maps location through the Google Maps API using an address.
        $address = $data['address'];
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ');
        $output= json_decode($geocode);
        $latitude = floatval($output->results[0]->geometry->location->lat);
        $longitude = floatval($output->results[0]->geometry->location->lng);
    }
    ?>
<script>
    var latitude = "<?= $latitude ?>";
    var longitude = "<?= $longitude ?>";
    var edit_marker = true;

</script>
<section class="admin-edit">
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="grid-x admin-edit-actions">
                <div class="cell small-6 medium-4 large-4 back-button"><img src="../img/arrowleft.svg"><span><a href="admin.php">Terug naar overzicht</a></span></div>
                <div class="cell small-6 medium-4 medium-offset-4 large-4 large-offset-4">
                    <div class="delete-button">
                        <a href="admin_delete.php?id= <?= $data['id'] ?>" onclick="return confirm('Are you sure?')">Verwijderen</a>
                    </div>
                </div>
            </div>
            <div class="admin-edit-block">
                <h1>Linkedin: Basis wijzigen</h1>
                <hr>
                <div class="admin-edit-block-form">
                    <form method="post" enctype="multipart/form-data">
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <label for="titel">Titel</label>
                                <input type="text" name="title" id="title" value="<?= $data['title'] ?>"><br>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <label for="datum">Datum</label>
                                <input type="datetime-local" name="date" id="date" value="<?= $data['date'] ?>" ><br>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <label for="file">Bestand</label>
                                <input type="file" name="myfile" id="myfile">
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <label for="location">Locatie</label>
                                <input type="location" name="location" class="location" value="<?= $data['location'] ?>" placeholder="Locatie">
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-12 large-12">
                                <textarea id="mytextarea" name="content"><?= $data['content'] ?></textarea>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <div class="search-field" id="floating-panel">
                                    <input id="address" type="textbox" placeholder="Locatie....">
                                    <input id="submit" type="button" value="Zoeken">
                                </div>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <div class="address-field">
                                    <p>Adres: <span id="found-address"><?= $data['address'] ?></span></p>
                                    <input type="hidden" value="<?= $data['address'] ?>" name="address" id="form_address">
                                </div>
                            </div>
                        </div>
                        <div class="admin-edit-block-maps" id="add_map"></div>
                        <div class="grid-x">
                            <div class="small-12 medium-4 medium-offset-8 large-4 large-offset-8">
                                <input type="hidden" name="id" value="<?= $data['id'] ?>">
                                <input type="submit" name="submit" class="edit-button" value="Updaten">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php include_once 'admin_footer.php'; ?>