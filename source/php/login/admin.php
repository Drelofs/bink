<?php 
$login_page = false;
$page = "login.php";
$add_edit = false;
include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once '../includes/functions.php';
include_once 'admin_header.php';
session_start();

//Check if admin is logged in or else go to login page
if(!$_SESSION['loggedin']){
    echo "<script type=\"text/javascript\"> location.replace('index.php'); </script>";
}
else{

    $results = [];

    try {
        $query = $db->prepare("SELECT * FROM `workshops`");
        $query->execute();
    } catch (PDOException $e) {
        $sMsg = '<p>
                Line: ' . $e->getLine() . '<br/>
                File: ' . $e->getFile() . '<br/>
                Message: ' . $e->getMessage() . '
                </p>';

        trigger_error($sMsg);
    }

    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        $results[] = [
            'id' => $row['id'],
            'title' => $row['title'],
            'date' => format_date($row['date']),
            'content' => $row['content']
        ];
    }
    // print_r($results);
    ?>
    <section class="admin-panel">
        <h2>Workshops</h2>
        <div class="grid-x align-center">
            <div class="small-12 medium-8 large-8">
                <div class="add-button">
                    <a href="admin_add.php">Nieuw workshop</a>
                </div>
                </div>
            <div class="small-12 medium-8 large-8">
                <div class="admin-panel-grid">
                    <?php for($i=0; $i<count($results); $i++){ ?>
                        <div class="admin-panel-grid-item">
                            <div class="grid-x">
                                <div class="small-12 medium-4 large-4">
                                    <div class="record-name">
                                        <span>Naam:</span>
                                        <div class="record-name-item">
                                            <a href="admin_detail.php?id=<?= $results[$i]['id'] ?>"><?= $results[$i]['title'] ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="small-12 medium-4 large-4">
                                    <div class="record-date">
                                        <span>Datum:</span>
                                        <div class="record-date-item">
                                            <span><?= $results[$i]['date'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="small-12 medium-4 large-4">
                                    <span>Acties</span>
                                    <div class="grid-x">
                                        <div class="small-12 medium-6 large-6">
                                            <a href="admin_edit.php?id=<?= $results[$i]['id'] ?>">Wijzigen</a>
                                        </div>
                                        <div class="small-12 medium-6 large-6">
                                            <a href="admin_delete.php?id=<?= $results[$i]['id'] ?>">Verwijderen</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </section>
<?php }
include_once 'admin_footer.php'; ?>