<?php 
$login_page = false;
$page = "login.php";
include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once '../includes/functions.php';
include_once 'admin_header.php';

$id = $_GET['id'];

try {
    $query = $db->prepare("
                SELECT * FROM `workshops` WHERE `id` = '$id'");

    $query->execute();
} catch (PDOException $e) {
    $sMsg = "<p>
            Line: " . $e->getLine() . "<br />
            File: " . $e->getFile() . "<br />
            Message: " . $e->getMessage() . "
            </p>";

    trigger_error($sMsg);
}

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $data = [
        'id' => $row['id'],
        'title' => $row['title'],
        'date' => format_date($row['date']),
        'address' => $row['address'],
        'location' => $row['location'],
        'content' => $row['content']
    ];
}

//Get all reservations belonging to this workshop
try {
    $query = $db->prepare("
                SELECT * FROM `reservations` WHERE `workshop_id` = '$id'");

    $query->execute();
} catch (PDOException $e) {
    $sMsg = "<p>
            Line: " . $e->getLine() . "<br />
            File: " . $e->getFile() . "<br />
            Message: " . $e->getMessage() . "
            </p>";

    trigger_error($sMsg);
}

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $reservations[] = [
        'name' => $row['name'],
        'email' => $row['email'],
        'level' => $row['level'],
        'phone' => $row['phone']
    ];
}

//Get Google Maps location through the Google Maps API using an address.
$address = $data['address'];
$prepAddr = str_replace(' ','+',$address);
$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ');
$output= json_decode($geocode);
$latitude = str_replace(',', '.', floatval($output->results[0]->geometry->location->lat));
$longitude = str_replace(',', '.', floatval($output->results[0]->geometry->location->lng));
$page_title = $data['title'];

?>
<section class="admin-detail">
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="grid-x admin-detail-actions">
                <div class="cell small-12 medium-4 large-4 back-button"><img src="../img/arrowleft.svg"><span><a href="admin.php">Terug naar overzicht</a></span></div>
                <div class="cell small-6 medium-4 large-4">
                    <div class="edit-button">
                        <a href="admin_edit.php?id=<?= $data['id'] ?>">Wijzigen</a>
                    </div>
                </div>
                <div class="cell small-6 medium-4 large-4">
                    <div class="delete-button">
                        <a href="admin_delete.php?id=<?= $data['id'] ?>" onclick="return confirm('Are you sure?')">Verwijderen</a>
                    </div>
                </div>
            </div>
            <div class="admin-detail-block">
                <h1><?= $data['title'] ?></h1>
                <hr>
                <div class="grid-x">
                    <div class="small-12 medium-6 large-6">
                        <span>Datum: <?= $data['date'] ?></span>
                    </div>
                    <div class="small-12 medium-6 large-6">
                        <span>Locatie: Rotterdam</span>
                    </div>
                </div>
                <hr>
                <div class="admin-detail-block-text">
                    <?= $data['content'] ?>
                </div>
                <div class="admin-detail-block-maps" id="map"></div>
                <hr>
                <div class="admin-detail-block-signups">
                    <h2>Aanmeldingen</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Naam</th>
                                <th>E-mail</th> 
                                <th>Telefoonnummer</th>
                                <th>Kennis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($i=0; $i<count($reservations); $i++){ ?>
                            <tr>
                                <td><?= $reservations[$i]['name'] ?></td>
                                <td><a href="mailto:<?= $reservations[$i]['email'] ?>"><?= $reservations[$i]['email'] ?></a></td> 
                                <td><?= $reservations[$i]['phone'] ?></td>
                                <td><?= $reservations[$i]['level'] ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var longitude = parseFloat("<?= $longitude ?>");
    var latitude = parseFloat("<?= $latitude ?>");

</script>
<?php include_once 'admin_footer.php'; ?>