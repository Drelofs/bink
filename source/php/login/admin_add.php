<?php 
$add_edit = true;
$login_page = false;

include_once '../includes/dbh.inc.php';
include_once '../includes/connect.php';
include_once 'admin_header.php';

$uploadDirectory = "../uploads/";

if(isset($_POST['submit'])){
    $title = htmlspecialchars($_POST['title']);
    $date = htmlspecialchars($_POST['date']);
    $content = htmlspecialchars($_POST['content']);
    $address = htmlspecialchars($_POST['address']);
    $location = htmlspecialchars($_POST['location']);

    $errors = []; // Store all foreseen and unforseen errors here

    $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions

    $fileName = $_FILES['myfile']['name'];
    $fileSize = $_FILES['myfile']['size'];
    $fileTmpName  = $_FILES['myfile']['tmp_name'];
    $fileType = $_FILES['myfile']['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));

    $imagePath = $uploadDirectory . basename($fileName);
    $uploadPath = $currentDir . $uploadDirectory; 

    if (! in_array($fileExtension,$fileExtensions)) {
        $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
    }

    if ($fileSize > 20000000) {
        $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
    }
    if (empty($errors)) {
        //Upload image
        $didUpload = move_uploaded_file($fileTmpName, $uploadDirectory.$fileName);
    }

    //Add workshop to database
    try {
        $query = $db->prepare("
                INSERT INTO `workshops` (`title`, `date`, `content`, `address`, `location`, `filepath`) VALUES (:title, :date, :content, :address, :location, :filepath)");
        $query->bindParam(':title', $title);
        $query->bindParam(':date', $date);
        $query->bindParam(':content', $content);
        $query->bindParam(':address', $address);
        $query->bindParam(':location', $location);
        $query->bindParam(':filepath', $imagePath);
        $query->execute();
        echo "<script type=\"text/javascript\"> location.replace('admin.php'); </script>";
    } catch (PDOException $e) {
        $sMsg = '<p>
            Line: ' . $e->getLine() . '<br/>
            File: ' . $e->getFile() . '<br/>
            Message: ' . $e->getMessage() . '
            </p>';

        trigger_error($sMsg);
    }
}
else { ?>
<section class="admin-edit">
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="grid-x admin-edit-actions">
                <div class="cell small-6 medium-4 large-4 back-button"><img src="../img/arrowleft.svg"><span><a href="admin.php">Terug naar overzicht</a></span></div>
                <div class="cell small-6 medium-4 medium-offset-4 large-4 large-offset-4">
                </div>
            </div>
            <div class="admin-edit-block">
                <h1>Toevoegen</h1>
                <hr>
                <div class="admin-edit-block-form">
                    <form method="post" enctype="multipart/form-data">
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <label for="titel">Titel</label>
                                <input type="text" name="title" id="title" placeholder="Titel"><br>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <label for="datum">Datum</label>
                                <input type="datetime-local" name="date" id="date" ><br>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <label for="file">Bestand</label>
                                <input type="file" name="myfile" id="myfile">
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <label for="location">Locatie</label>
                                <input type="location" name="location" class="location" placeholder="Locatie">
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-12 large-12">
                                <textarea name="content" id="mytextarea"></textarea>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <div class="search-field" id="floating-panel">
                                    <input id="address" type="textbox" placeholder="Adres....">
                                    <input id="submit" type="button" value="Zoeken">
                                </div>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <div class="address-field">
                                    <p>Adres: <span id="found-address"></span></p>
                                    <input type="hidden" value="" name="address" id="form_address">
                                </div>
                            </div>
                        </div>
                        <div class="admin-edit-block-maps" id="add_map"></div>
                        <div class="grid-x">
                            <div class="small-12 medium-4 medium-offset-8 large-4 large-offset-8">
                                <input type="submit" name="submit" class="edit-button" value="Toevoegen">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } include_once 'admin_footer.php'; ?>