<?php 
$home_page = true;
$page = "index.php";
include_once 'includes/dbh.inc.php';
include_once 'includes/connect.php';
include_once 'includes/functions.php';


$url = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
// print_r($url);
// exit();

$data = [];

try {
    $query = $db->prepare("SELECT * FROM `workshops`");
    $query->execute();
} catch (PDOException $e) {
    $sMsg = '<p>
            Line: ' . $e->getLine() . '<br/>
            File: ' . $e->getFile() . '<br/>
            Message: ' . $e->getMessage() . '
            </p>';

    trigger_error($sMsg);
}

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $data[] = [
        'id' => $row['id'],
        'title' => $row['title'],
        'date' => format_date($row['date']),
        'content' => $row['content'],
        'location' => $row['location'],
        'filepath' => str_replace("../", "", $row['filepath'])
    ];
}

$header_image = 'img/bink-header.jpg';
include_once 'header.php';
?>

<section class="introduction grid-container">
    <div class="grid-x">
        <div class="cell small-12 medium-6 large-6">
            <div class="introduction-image hideme">
                <img src="img/portrait.jpg">
            </div>
        </div>
        <div class="cell small-12 medium-6 large-6 grid-margin-y introduction-content hideme">
            <div class="introduction-content-text" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical">
                <h2>Een workshop voor iedereen</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget diam 
                ac lorem posuere cursus et sit amet sapien. Donec enim enim, luctus ut laoreet 
                sit amet, elementum ut massa. </p> 
                <p>Ut sed pulvinar leo. Donec venenatis placerat eleifend. 
                Vestibulum pretium quis velit non condimentum. Sed fermentum pellentesque mi in cursus. 
                Donec venenatis ligula placerat magna elementum commodo. Duis suscipit nunc augue, 
                et pharetra tortor accumsan id.</p>
            </div>
        </div>
    </div>
</section>
<section class="text-block">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <div class="text-block-container">
                <h2>Title</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                    posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                    magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                    nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                    lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                    condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
            </div>
        </div>
    </div>
</section>
<section class="about-bink">
    <div class="about-bink-image-overlay" data-paroller-factor="0.2" data-paroller-factor-xs="0.2">
        <div class="grid-x">
            <div class="small-12 medium-5 medium-offset-6 large-5 large-offset-6">
                <div class="about-bink-container">
                    <div class="about-bink-container-text">
                        <h2>Over Bink</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                            posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                            magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                            nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                        <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                            lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                            condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
                    </div>
                    <div class="about-bink-link">
                        <a href="overbink.php">Lees meer</a><div class="about-bink-link-image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="workshops hideme">
    <h2>Opkomende workshops</h2>
    <div class="grid-x">
        <?php for($i=0; $i<3; $i++){ ?>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php?id=<?= $data[$i]['id'] ?>">
                    <div class="workshop-item" style="background-image: url('<?php echo $data[$i]['filepath']; ?>')">
                        <span class="workshop-item-location"><?= $data[$i]['location'] ?></span>
                        <span class="workshop-item-date"><?= $data[$i]['date'] ?></span>
                        <div class="workshop-item-overlay">
                            <h3><?= $data[$i]['title'] ?></h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?php } ?>
        <!-- <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/amsterdam.jpg')">
                        <span class="workshop-item-location">Amsterdam</span>
                        <span class="workshop-item-date">5 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Advanced</h3>
                            <p>Een workshop over het geadvanceerd gebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="cell small-12 medium-4 large-4">
            <div class="workshop-container">
                <a href="detail.php">
                    <div class="workshop-item" style="background: url('img/denhaag.jpg')">
                        <span class="workshop-item-location">Den Haag</span>
                        <span class="workshop-item-date">17 januari 2019</span>
                        <div class="workshop-item-overlay">
                            <h3>Linkedin: Basis</h3>
                            <p>Een workshop over het basisgebruik van Linkedin</p>
                            <div class="workshop-item-overlay-link">
                                <img src="img/arrowright.svg">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div> -->

    </div>
    <?php if($home_page) : ?>
    <div class="grid-x">
        <div class="small-12 medium-6 medium-offset-6 large-4 large-offset-8">
            <div class="grid-x">
                <div class="small-6 small-offset-6 medium-6 medium-offset-6 large-6 large-offset-6">
                        <div class="latest-workshops-cta">
                            <a class="latest-workshops-cta-link" href="workshops.php">Meer workshops</a>
                            <img src="img/arrowrightlink.svg">
                        </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</section>
<?php include_once 'footer.php';
