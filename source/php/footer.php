</div>
<footer>
    <div class="grid-x footer-block">
        <div class="cell small-12 medium-3 large-3">
            <div class="footer-container">
                <img class="footer-container-logo"src="img/bink-white.png">
            </div>
        </div>
        <div class="cell small-12 medium-2 large-2">
            <div class="footer-block-links">
                <h4>Links</h3>
                <ul>
                    <li><a href="link1.php">Link 1</a></li>
                    <li><a href="link2.php">Link 2</a></li>
                    <li><a href="link3.php">Link 3</a></li>
                    <li><a href="link4.php">Link 4</a></li>
                </ul>
            </div>
        </div>
        <div class="cell small-12 medium-2 large-2">
            <div class="footer-block-links">
                <h4>Links</h3>
                <ul>
                    <li><a href="link1.php">Link 1</a></li>
                    <li><a href="link2.php">Link 2</a></li>
                    <li><a href="link3.php">Link 3</a></li>
                    <li><a href="link4.php">Link 4</a></li>
                </ul>
            </div>
        </div>
        <div class="cell small-12 medium-2 large-2">
            <div class="footer-block-links">
                <h4>Links</h3>
                <ul>
                    <li><a href="link1.php">Link 1</a></li>
                    <li><a href="link2.php">Link 2</a></li>
                    <li><a href="link3.php">Link 3</a></li>
                    <li><a href="link4.php">Link 4</a></li>
                </ul>
            </div>
        </div>
        <div class="cell small-12 medium-3 large-3">
            <div class="footer-block-social">
                <h4>Social Media</h4>
                <ul>
                    <li><a href="www.facebook.com"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href="www.twitter.com"><i class="fab fa-twitter-square"></i></a></li>
                    <li><a href="www.linkedin.com"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="footer-block-copyright">
                <small>Copyright</small>
            </div>
        </div>
    </div>
</footer>
<script src="js/vendor.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pwrr3nti5avcjbprgp1t15o9fza69ze0bt4i4cse2bjeevc4"></script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnrwTJB7lesmzy_aqSa1k_xMWKz5J3DpQ&callback=initMap">
</script>
<script src="js/scripts.min.js"></script>
</body>
</html>