<?php
include_once 'includes/dbh.inc.php';
include_once 'includes/connect.php';
include_once 'includes/functions.php';
session_start();

$name = $_POST['name'];
$email = $_POST['email'];
$level = $_POST['level'];
$phone = $_POST['phone'];
$workshop_id = $_POST['workshop_id'];

if($name && $email && $level){
    try {
        $sql = "INSERT INTO `reservations` (`name`, `email`, `level`, `phone`, `workshop_id`) VALUES (?,?,?,?,?)";
        $query = $db->prepare($sql);
        $query->execute([$name, $email, $level, $phone, $workshop_id]);
        $_SESSION['registered'] = true;
        echo "<script type=\"text/javascript\"> location.replace('detail.php?id=".$workshop_id."'); </script>";

    } catch (PDOException $e) {
        $sMsg = '<p>
            Line: ' . $e->getLine() . '<br/>
            File: ' . $e->getFile() . '<br/>
            Message: ' . $e->getMessage() . '
            </p>';

        trigger_error($sMsg);
    }
}
else{
    $_SESSION['registered'] = true;
    echo "<script type=\"text/javascript\"> location.replace('detail.php?id=".$workshop_id."'); </script>";
}