$( document ).ready(function() {

    $(window).scroll(function() {
        const scrollTop     = $(window).scrollTop(),
            elementOffset = $('.wrapper').offset().top;
        if (elementOffset < document.documentElement.scrollTop + $('.top-bar').height()) {
            $('.top-bar').addClass('scrolled');
        }
        else {
            $('.top-bar').removeClass('scrolled');
        }

        $('.hideme').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object - 300 ){
                
                $(this).animate({'opacity':'1'},600);
                    
            }
            
        }); 
    });
    $('.image-overlay').paroller(); 
    $('.introduction-content-text').paroller();
    $('.about-bink-image-overlay').paroller();

});

function initMap() {
    var location = {lat: latitude, lng: longitude};
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 15, center: location, disableDefaultUI: true});
    var marker = new google.maps.Marker({position: location, map: map});

}

function initEditMap() {
    // if(!longitude && !latitude){
    //     latitude = 51.9236445;
    //     longitude = 4.4679457;
    // }
    latitude = parseFloat(51.9236445);
    longitude = parseFloat(4.4679457);
    var map = new google.maps.Map(document.getElementById('add_map'), {
      zoom: 15,
      center: {lat: latitude, lng: longitude}
    });
    var geocoder = new google.maps.Geocoder();
    // if(edit_marker){
    //     geocodeAddress(geocoder, map);
    // }
    document.getElementById('submit').addEventListener('click', function() {
      geocodeAddress(geocoder, map);
    });
}

function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        var foundAddress = results[0].formatted_address;
        document.getElementById('found-address').innerHTML = foundAddress;
        document.getElementById('form_address').value = foundAddress;
        console.log(results[0]);
        var marker = new google.maps.Marker({
            map: resultsMap,
            position: results[0].geometry.location
        });
        } else {
        alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

tinymce.init({
    selector: '#mytextarea'
  });