<?php 
$login_page = true;
$page = "login.php";
include_once 'admin_header.php';
?>
<section class="login-page">
    <div class="login-page-panel">
        <h2>Inloggen</h2>
        <form action="login_process.php" method="post">
        <div class="login-page-panel-fields">
            <input type="text" name="field1" placeholder="Username">
            <input type="password" name="field2" placeholder="Password">
            <input type="submit" name="submit" class="login-button" text="submit">
        </div>
        </form>
    </div>
</section>
<?php include_once 'admin_footer.php'; ?>
