<?php 
$add_edit = true;
$login_page = false;
$page = "login.php";
include_once 'admin_header.php';
?>
<section class="admin-edit">
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="grid-x admin-edit-actions">
                <div class="cell small-6 medium-4 large-4 back-button"><img src="img/arrowleft.svg"><span><a href="admin.php">Terug naar overzicht</a></span></div>
                <div class="cell small-6 medium-4 medium-offset-4 large-4 large-offset-4">
                    <div class="delete-button">
                        <a href="admin_delete.php" onclick="return confirm('Are you sure?')">Verwijderen</a>
                    </div>
                </div>
            </div>
            <div class="admin-edit-block">
                <h1>Linkedin: Basis wijzigen</h1>
                <hr>
                <div class="admin-edit-block-form">
                    <form action="?action=save" method="post">
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <label for="titel">Titel</label>
                                <input type="text" name="title" id="title" value="Title"><br>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <label for="datum">Datum</label>
                                <input type="datetime-local" name="date" id="date" ><br>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-12 large-12">
                                <textarea id="mytextarea">Hello, World!</textarea>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="small-12 medium-6 large-6">
                                <div class="search-field" id="floating-panel">
                                    <input id="address" type="textbox" placeholder="Locatie....">
                                    <input id="submit" type="button" value="Zoeken">
                                </div>
                            </div>
                            <div class="small-12 medium-6 large-6">
                                <div class="address-field">
                                    <p>Adres: <span id="found-address"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="admin-edit-block-maps" id="add_map"></div>
                        <div class="grid-x">
                            <div class="small-12 medium-4 medium-offset-8 large-4 large-offset-8">
                                <input type="submit" class="edit-button" value="Updaten">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once 'admin_footer.php'; ?>