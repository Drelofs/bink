<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="Bink, training, advies">
        <meta name="author" content="Drelofs">
        <title>Bink training & Advies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="css/main.min.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    </head>
    <body>
        <section class="main-header">
            <div class="image-overlay" data-paroller-factor="0.5" data-paroller-factor-xs="0.2">
                <?php if($home_page) {?>
                <img src="img/bink_white.svg">
                <?php } else{ ?>
                <h2><?php echo $page_title; ?></h2>
                <?php } ?>
            </div>
            <nav class="top-bar">
                <a href="index.php" class="main-logo">
                    <img src="img/bink-blue.png">
                </a>
                <div class="top-bar-section">
                    <ul class="menu-items right">
                        <li class="menu-item"><a class="menu-link" href="workshops.php">Workshops</a></li>
                        <li class="menu-item"><a class="menu-link" href="over.php">Over Bink</a></li>
                        <li class="menu-item"><a class="menu-link" href="contact.php">Contact</a></li>
                    </ul>
                </div>
            </nav>
        </section>
        <div class="wrapper">