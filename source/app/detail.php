<?php 
$page = 'workshops.php';
$page_title = 'Linkedin Basis';
include_once 'header.php';
?>
<section class="date-location">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <div class="grid-x date-location-block">
                <div class="cell small-12 medium-6 date-left"><b>Datum: </b>28 december 2018 16:00</div>
                <!-- <div class="cell auto"></div> -->
                <div class="cell small-12 medium-6 location-right"><b>Locatie: </b>Rotterdam</div>
            </div>
        </div>
    </div>
</section>
<section class="text-block">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            <div class="text-block-container">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                    posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                    magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                    nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                    lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                    condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                    posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                    magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                    nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                    lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                    condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
            </div>
        </div>
    </div>
</section>
<section class="google-maps-block" id="map"></section>
<section class="form-block">
    <div class="grid-x align-center">
        <div class="cell small-12 medium-6 large-6">
            
        </div>
    </div>
</section>
<?php include_once 'footer.php';