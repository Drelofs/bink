<?php 
$login_page = false;
$page = "login.php";
include_once 'admin_header.php';
?>
<section class="admin-panel">
    <h2>Workshops</h2>
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="add-button">
                <a href="admin_add.php">Nieuw workshop</a>
            </div>
            </div>
        <div class="small-12 medium-8 large-8">

            <div class="admin-panel-grid">
                <div class="admin-panel-grid-item">
                    <div class="grid-x">
                        <div class="small-12 medium-4 large-4">
                            <div class="record-name">
                                <span>Naam:</span>
                                <div class="record-name-item">
                                    <a href="admin_detail.php">Linkedin: Basis</a>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <div class="record-date">
                                <span>Datum:</span>
                                <div class="record-date-item">
                                    <span>13 januari 2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <span>Acties</span>
                            <div class="grid-x">
                                <div class="small-12 medium-6 large-6">
                                    <a href="edit.php">Wijzigen</a>
                                </div>
                                <div class="small-12 medium-6 large-6">
                                    <a href="delete.php">Verwijderen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="admin-panel-grid-item">
                    <div class="grid-x">
                        <div class="small-12 medium-4 large-4">
                            <div class="record-name">
                                <span>Naam:</span>
                                <div class="record-name-item">
                                    <a href="admin_detail.php">Linkedin: Basis</a>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <div class="record-date">
                                <span>Datum:</span>
                                <div class="record-date-item">
                                    <span>13 januari 2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <span>Acties</span>
                            <div class="grid-x">
                                <div class="small-12 medium-6 large-6">
                                    <a href="edit.php">Wijzigen</a>
                                </div>
                                <div class="small-12 medium-6 large-6">
                                    <a href="delete.php">Verwijderen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="admin-panel-grid-item">
                    <div class="grid-x">
                        <div class="small-12 medium-4 large-4">
                            <div class="record-name">
                                <span>Naam:</span>
                                <div class="record-name-item">
                                    <a href="admin_detail.php">Linkedin: Basis</a>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <div class="record-date">
                                <span>Datum:</span>
                                <div class="record-date-item">
                                    <span>13 januari 2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <span>Acties</span>
                            <div class="grid-x">
                                <div class="small-12 medium-6 large-6">
                                    <a href="edit.php">Wijzigen</a>
                                </div>
                                <div class="small-12 medium-6 large-6">
                                    <a href="delete.php">Verwijderen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="admin-panel-grid-item">
                    <div class="grid-x">
                        <div class="small-12 medium-4 large-4">
                            <div class="record-name">
                                <span>Naam:</span>
                                <div class="record-name-item">
                                    <a href="admin_detail.php">Linkedin: Basis</a>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <div class="record-date">
                                <span>Datum:</span>
                                <div class="record-date-item">
                                    <span>13 januari 2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-4 large-4">
                            <span>Acties</span>
                            <div class="grid-x">
                                <div class="small-12 medium-6 large-6">
                                    <a href="edit.php">Wijzigen</a>
                                </div>
                                <div class="small-12 medium-6 large-6">
                                    <a href="delete.php">Verwijderen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php include_once 'admin_footer.php'; ?>