<?php 
$login_page = false;
$page = "login.php";
include_once 'admin_header.php';
?>
<section class="admin-detail">
    <div class="grid-x align-center">
        <div class="small-12 medium-8 large-8">
            <div class="grid-x admin-detail-actions">
                <div class="cell small-12 medium-4 large-4 back-button"><img src="img/arrowleft.svg"><span><a href="admin.php">Terug naar overzicht</a></span></div>
                <div class="cell small-6 medium-4 large-4">
                    <div class="edit-button">
                        <a href="admin_edit.php">Wijzigen</a>
                    </div>
                </div>
                <div class="cell small-6 medium-4 large-4">
                    <div class="delete-button">
                        <a href="admin_delete.php" onclick="return confirm('Are you sure?')">Verwijderen</a>
                    </div>
                </div>
            </div>
            <div class="admin-detail-block">
                <h1>Linkedin: Basis</h1>
                <hr>
                <div class="grid-x">
                    <div class="small-12 medium-6 large-6">
                        <span>Datum: 13 januari 2019</span>
                    </div>
                    <div class="small-12 medium-6 large-6">
                        <span>Locatie: Rotterdam</span>
                    </div>
                </div>
                <hr>
                <div class="admin-detail-block-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                        posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                        magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                        nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                    <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                        lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                        condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tortor, lobortis quis sodales id, 
                        posuere a elit. Donec tincidunt, diam non pulvinar dictum, tellus sem tristique ante, id maximus neque 
                        magna pulvinar erat. Sed non elit ultrices ex feugiat efficitur pulvinar sed orci. Nunc ultricies fermentum metus, 
                        nec tristique sem pulvinar a. Morbi sagittis risus ut faucibus feugiat. Donec ornare ullamcorper lectus. </p>
                    <p>A semper ante condimentum non. Quisque convallis, ligula et suscipit lacinia, orci purus efficitur augue, in sodales justo 
                        lacus nec lorem. Sed posuere libero eget orci semper, quis ullamcorper sapien lacinia. Morbi laoreet, quam at 
                        condimentum dignissim, lectus erat rutrum lectus, vitae rhoncus orci tortor vel velit.</p>
                </div>
                <div class="admin-detail-block-maps" id="map"></div>
                <hr>
                <div class="admin-detail-block-signups">
                    <h2>Aanmeldingen</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Naam</th>
                                <th>E-mail</th> 
                                <th>Telefoonnummer</th>
                                <th>Kennis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Julia Lanu</td>
                                <td>julialanu@gmail.com</td> 
                                <td>0612345678</td>
                                <td>Slecht</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once 'admin_footer.php'; ?>