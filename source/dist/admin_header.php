<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="Bink, training, advies">
        <meta name="author" content="Drelofs">
        <title>Bink Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="css/main.min.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    </head>
    <body>
        <?php if (!$login_page) { ?>
        <section class="main-header">
            <!-- <div class="image-overlay" data-paroller-factor="0.5" data-paroller-factor-xs="0.2">
                <h2><?php echo $page_title; ?></h2>
            </div> -->
            <nav class="admin-bar">
                <img src="img/bink_white.svg">
            </nav>
        </section>
        <?php } ?>